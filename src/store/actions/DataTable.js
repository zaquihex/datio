import * as actionTypes from './actionTypes';

export const getDataInit = () => {
    return {
        type: actionTypes.GET_DATA_INIT
    }
}

export const getDataStart = () => {
    return {
        type: actionTypes.GET_DATA_START
    }
}

export const getDataSuccess = (films) => {
    return {
        type: actionTypes.GET_DATA_SUCCESS,
        films: films
    }
}

export const getDataFail = (error) => {
    return {
        type: actionTypes.GET_DATA_FAIL,
        error: error
    }
}