export const GET_DATA = 'GET_DATA'
export const GET_DATA_START = 'GET_DATA_START';
export const GET_DATA_SUCCESS = 'GET_DATA_SUCCESS';
export const GET_DATA_FAIL = 'GET_DATA_FAIL';
export const GET_DATA_INIT = 'GET_DATA_INIT';

export const CHANGE_TAB_INDEX = 'CHANGE_TAB_INDEX';