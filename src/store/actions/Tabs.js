import * as actionTypes from './actionTypes';

export const changeTabIndex = (newTabIndex) => {
    return {
        type: actionTypes.CHANGE_TAB_INDEX,
        tabIndex: newTabIndex
    }
}