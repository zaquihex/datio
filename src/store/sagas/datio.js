import { put } from "redux-saga/effects";

import { key } from '../../components/Constants';

import axios from "../../axios";
import * as actions from "../actions";

//function to call the API "themoviedb" and fill the datatable
export function* getDataSaga(action) {
  try {
    yield put(actions.getDataStart());
    const response = yield axios.get('https://api.themoviedb.org/3/discover/movie?api_key='+key);
    yield put(actions.getDataSuccess(response.data.results));
  } catch (error) {
    yield put(actions.getDataFail(error));
  }
}