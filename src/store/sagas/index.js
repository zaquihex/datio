import { takeEvery } from 'redux-saga/effects';

import * as actionTypes from '../actions/actionTypes';

import {getDataSaga} from './datio';
//function listening to fill the table with data when the application start
export function* watchDatio() {
    yield takeEvery(actionTypes.GET_DATA_INIT, getDataSaga);
  }