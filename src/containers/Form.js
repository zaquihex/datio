import React, { Component } from 'react';

import Tabs from '../components/Tabs/TabsComponent';

import Modal from '../components/Modal/Modal';
import FormComponent from '../components/Form/FormComponent';

//Class to render the Form Container using: Tabs to render the menú, formComponent for the structure and Modal Component for the messages
class Form extends Component {

    state = {
        connectMessageActive: false,
        message: null
    }

    setConnectMessage = (newValue, message) => {
        this.setState({ connectMessageActive: newValue, message: message });
    }

    contact = (message) => {
            this.setConnectMessage(true, message);
    }

    render() {
        const tabs = [{ title: 'Datatable', content: null }, { title: 'Form', content: <FormComponent contact={this.contact} /> }];
        return (
            <div>
                <Modal active={this.state.connectMessageActive} modalClosed={() => { this.setConnectMessage(false, null) }}>
                    {this.state.message}
                </Modal>
                <Tabs key='Form_Tabs' tabs={tabs} indexTab={1} />
            </div>
        );
    }
}

export default Form;