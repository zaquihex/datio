import React, { Component } from 'react';

import * as actions from '../store/actions/';
import { connect } from "react-redux";

import Tabs from '../components/Tabs/TabsComponent';
import Table from '../components/Table/Table';

import Spinner from '../components/Spinner/Spinner';

//Class to render: 1. loading spinner if the call has not finished yet. 2. error message if there was an error with the call. 3. the datatable
class DataTable extends Component {

    getColumns = () => {
        return [{
            dataField: 'id',
            text: 'ID',
            sort: true
        }, {
            dataField: 'original_title',
            text: 'Title',
            sort: true
        }, {
            dataField: 'popularity',
            text: 'Popularity',
            sort: true
        }, {
            dataField: 'vote_average',
            text: 'Score',
            sort: true
        }, {
            dataField: 'release_date',
            text: 'Release Date',
            sort: true
        }];
    }

    render() {
        const columns = this.getColumns();
        const tabs = [{ title: 'Datatable', content: <Table columns={columns} data={this.props.films} /> }, { title: 'Form', content: null }];
        return (
            <>
                {this.props.loading ? <Spinner /> : null}
                {
                    this.props.error !== null ?
                        <div style={{ display: 'inline-flex' }}>
                            <i class="fa fa-exclamation-triangle"></i>
                            <div>Sorry, There are problems with the connection</div>
                        </div>
                        :
                        <Tabs key='DataTable_Tabs' tabs={tabs} indexTab={0} />
                }

            </>
        );
    }
}

const mapStateToProps = state => {
    return {
        loading: state.loading,
        films: state.films,
        error: state.error
    }
}

export default connect(mapStateToProps, null)(DataTable);