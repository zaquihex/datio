import React from 'react';
import { Route, Switch } from 'react-router-dom';

import { connect } from "react-redux";

//import Containers
import Form from './containers/Form';
import DataTable from './containers/DataTable';
import Header from './components/Header/Header';

import * as actions from './store/actions/';

//import styles
import './App.scss';
import '../node_modules/font-awesome/css/font-awesome.min.css';

class App extends React.Component {

  componentDidMount = () => {
    this.props.getData();
  }

  render() {
    return (
      <div className='App'>
        <Header className="header" />
        <Switch>
          <Route path="/Form" exact component={Form} />
          <Route path="/" component={DataTable} />
        </Switch>
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    getData: () => {
      dispatch(actions.getDataInit())
    }
  }
}

export default connect(null, mapDispatchToProps)(App);
