import React, { Component } from 'react';

import { connect } from "react-redux";
import { Redirect } from 'react-router-dom'

import * as actions from '../../store/actions';

import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import "../../../node_modules/react-tabs/style/react-tabs.css";

//This class needs a list of tabs with the title and the content
//example: tabs = [{ title: 'tab1', content: 'content 1' }, { title: 'tab2', content: 'content 2' }]
class TabsComponent extends Component {

    state = {
        tabToRedirect: null
    }
    //redirect when the user change the tab
    setTabIndex = (index, lastIndex) => {
        let tabToRedirect;
        if (index === 0) { tabToRedirect = '/' }
        else { tabToRedirect = '/Form' }
        this.setState({ tabToRedirect })
    }

    render = () => {
        const { tabs } = this.props;
        const tabList = tabs.map((tab, index) => <Tab key={"tabList-" + index}>{tab.title}</Tab>);
        const tabPanel = tabs.map((tab, index) => <TabPanel key={"tabPanel-" + index}>{tab.content}</TabPanel>);
        return (
            <>
                {this.state.tabToRedirect !== null ? <Redirect to={this.state.tabToRedirect}/> : null }
                <Tabs selectedIndex={this.props.indexTab} onSelect={this.setTabIndex}>
                    <TabList>
                        {tabList}
                    </TabList>
                    {tabPanel}
                </Tabs>
            </>);
    }
}


const mapStateToProps = state => {
    return {
        tabIndex: parseInt(state.tabIndex)
    }
}

const mapDispatchToProps = dispatch => {
    return {
        changeTabIndex: (newTabIndex) => {
            dispatch(actions.changeTabIndex(newTabIndex))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(TabsComponent);