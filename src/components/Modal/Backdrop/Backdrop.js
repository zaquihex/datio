import React from 'react';

import './Backdrop.scss';
//Basic Backdrop class used by the Modal
const backdrop = (props) => (props.active ? <div className='Backdrop' onClick={props.clicked}></div> : null);

export default backdrop;