import React, { Component } from 'react';

import './Modal.scss';
import Backdrop from './Backdrop/Backdrop';
//Modal component to show a message like a popup or notification
class Modal extends Component {

    shouldComponentUpdate(nextProps, nextState) {
        return nextProps.show !== this.props.show || nextProps.children !== this.props.children;
    }

    render = () => (
        <>
            <Backdrop active={this.props.active} clicked={this.props.modalClosed} />
            <div
                className='Modal'
                style={{
                    transform: this.props.active ? 'translateY(0)' : 'translateY(+100vh)',
                    opacity: this.props.active ? '1' : '0'
                }}>
                {this.props.children}
            </div>
        </>
    )

}

export default Modal;