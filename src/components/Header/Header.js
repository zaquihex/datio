import React from 'react';
import './Header.scss';
import datioIcon from '../../assets/datioIcon.png';
//Header Component
const Header = () => (
    <div className='HeaderStyle'>
        <div className="HeaderIcon">
            <img src={datioIcon} alt='datioIcon' height="20" width="70"></img>
        </div>

        <h2>Datio Test</h2>
        <div className="HeaderSubtitle"> Datatable & Form </div>
        <hr />
    </div>);

export default Header;