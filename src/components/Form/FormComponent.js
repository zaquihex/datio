import React, { Component } from 'react';

import './Form.scss';

class FormComponent extends Component {

    state = {
        name: '',
        middleName: '',
        lastName: '',
        email: '',
    }

    handleChange = (event) => {
        switch (event.target.name) {
            case 'name': this.setState({ name: event.target.value }); break;
            case 'middleName': this.setState({ middleName: event.target.value }); break;
            case 'lastName': this.setState({ lastName: event.target.value }); break;
            case 'email': this.setState({ email: event.target.value }); break;
            default: break;
        }
    }

    contact = () => {
        if (this.state.name.length > 0 && this.state.email.length > 0) {
            const message = 'Thanks ' + this.state.name + ', an email has been sent to Datio. We will send you an email to ' + this.state.email + ' as soon as possible';
            this.props.contact(message);
        }
        else{
            this.props.contact('Please,fill in the "name" and "email" fields to contact with us');
        }

    }

    render = () => {
        return (
            <form>
                <h1>Contact Form</h1>
                <div className="Form_contactLine">
                    <p>Please fill in this form to create an account (</p><p className="Mandatory"> *</p><p>Mandatory fields)</p>
                </div>
                <hr />
                <div className="Form_inputStyles">
                    <div><label htmlFor="name"><b>Name</b><b className="Mandatory">*</b></label>
                        <input value={this.state.name} onChange={this.handleChange} type="text" placeholder="name" name="name" required />
                    </div>
                    <div><label htmlFor="middleName"><b>Middle Name</b></label>
                        <input value={this.state.middleName} onChange={this.handleChange} type="text" placeholder="middleName" name="middleName" /></div>

                    <div><label htmlFor="lastName"><b>Last Name</b></label>
                        <input value={this.state.lastName} onChange={this.handleChange} type="text" placeholder="lastName" name="lastName" /></div>

                    <div><label htmlFor="email"><b>Email</b><b className="Mandatory">*</b></label>
                        <input value={this.state.email} onChange={this.handleChange} type="text" placeholder="email" name="email" required /></div>

                </div>

                <hr />

                <button type="button" className="Form_contactButton" onClick={this.contact}>Contact</button>
            </form>
        )
    }

};

export default FormComponent;