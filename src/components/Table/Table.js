import React from 'react';

import BootstrapTable from 'react-bootstrap-table-next';
import paginationFactory from 'react-bootstrap-table2-paginator';

import 'react-bootstrap-table-next/dist/react-bootstrap-table2.min.css';
import 'react-bootstrap-table2-paginator/dist/react-bootstrap-table2-paginator.min.css';
//Component that it can be used by other classes because "columns" and "data" are parameters
const Table = props => (
    <BootstrapTable key='DataTable_BootstrapTable' keyField='id' data={props.data} columns={props.columns} pagination={paginationFactory()} />
)

export default Table;