import React from 'react';
//import DOM
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
//import redux
import { Provider } from "react-redux";
import { createStore, applyMiddleware, compose } from "redux";
import thunk from "redux-thunk";
import createSagaMiddleware from "redux-saga";

import registerServiceWorker from './registerServiceWorker';

import datioReducer from './store/reducers/datio';
import { watchDatio } from './store/sagas';

import App from './App';
//import styles
import './index.css';
import 'bootstrap/dist/css/bootstrap.min.css';

//Change "!==" to "===" to debug redux (redux extension is necessary) 
const composeEnhancers =
    process.env.NODE_ENV !== "development"
        ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
        : null || compose;

const sagaMiddleware = createSagaMiddleware();

const store = createStore(
    datioReducer,
    composeEnhancers(applyMiddleware(thunk, sagaMiddleware))
);

sagaMiddleware.run(watchDatio);

const app = (
    <Provider store={store}>
        <BrowserRouter>
            <App />
        </BrowserRouter>
    </Provider>
);

ReactDOM.render(app, document.getElementById('root'));
registerServiceWorker();
