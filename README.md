#Owner
This Project has been created by Zaquiel Rodriguez Arce
#Datio project
This project has been created for the datio test

#Application
The application has a Header and a body with two tabs.

   #Header
    1. Simple Header with gradient style, image and a specific font-family
   #Datatable tab - info
    1. data get from themoviedb api
    2. the component has been created with BootstrapTable (user can order the data and it has pagination)
    3. It has a loading message (Spinner) while the component is waiting for the reponse from themovidedb.
    4. It shows an error message instead the table if there has been some issue with the call.
   #Form tab - info
    1. Basic form with the input information saved on the state of the class
    2. Some input fields are mandatory and the application check their values before "send" the message (it does not send any message, form only shows a message).
    3.A generic message is shown when the user clicks on "Contact" button

